{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Data.Text (Text)
import Data.Monoid ((<>))
import Control.Applicative ((<*>))
import Data.Time (UTCTime, getCurrentTime)
import Data.Configurator (Worth(..))
import qualified Data.Configurator as Conf
import Control.Monad.Trans.Resource (runResourceT)
import Facebook
       (Credentials(..), runFacebookT, AppAccessToken, getAppAccessToken,
        FacebookException(..))
import Network.HTTP.Conduit
       (newManager, tlsManagerSettings, Manager)
import System.Log.FastLogger
import Options.Applicative
import Database.SQLite.Simple
import Control.Exception
import NSLogger

import Grab


data FBPage = FBPage
    { fbPageId :: Text
    , fbPageCreatedTime :: UTCTime
    , fbPageUpdatedTime :: UTCTime
    } deriving (Eq,Show,Ord)


instance FromRow FBPage where
    fromRow = FBPage <$> field <*> field <*> field

data CrawlerArgs = CrawlerArgs
    { caDBPath :: FilePath
    , caLogPath :: FilePath
    , caCfgPath :: FilePath
    }

crawlerArgs :: Parser CrawlerArgs
crawlerArgs =
    CrawlerArgs <$>
    strOption
        (long "dbpath" <> metavar "FILE" <> help "Path to SQLite db file") <*>
    strOption (long "logpath" <> metavar "FILE" <> help "Path to log folder")<*>
    strOption (long "cfgpath" <> metavar "FILE" <> help "Path to cfg file")


getAppToken :: Manager -> Credentials -> IO AppAccessToken
getAppToken manager creds =
    runResourceT $ runFacebookT creds manager getAppAccessToken


getCredentials :: FilePath -> IO Credentials
getCredentials cfgpath = do
    config <- Conf.load [Required cfgpath]
    appName' <- Conf.require config "fb.app_name"
    appId' <- Conf.require config "fb.app_id"
    appSecret' <- Conf.require config "fb.app_secret"
    return
        Credentials
        { appName = appName'
        , appId = appId'
        , appSecret = appSecret'
        }

opts =
    info
        (helper <*> crawlerArgs)
        (fullDesc <> progDesc "Grab info from registered in db pages" <>
         header "sna-hcrawler - social network analysis crawler")

main :: IO ()
main = do
    ca <- execParser opts
    utctime <- getCurrentTime
    loggerset <-
        newFileLoggerSet defaultBufSize (caLogPath ca <> "/crawler.log")
    creds <- getCredentials (caCfgPath ca)
    manager <- newManager tlsManagerSettings
    tok <- getAppToken manager creds
    conn <- open (caDBPath ca)
    fbpages <- query_ conn "SELECT * FROM fbPage" :: IO [FBPage]
    mapM_
        (\fbpage ->
              catch
                  (grabFbPage
                       loggerset
                       creds
                       tok
                       manager
                       (fbPageId fbpage)
                       (fbPageUpdatedTime fbpage))
                  (\e ->
                        case e of
                            FacebookException _ _ -> do
                                pushLogStrLn
                                    loggerset
                                    (logNS utctime ERROR (show e))
                                execute
                                    conn
                                    "DELETE FROM fbPage WHERE pid = ?"
                                    (Only (fbPageId fbpage))
                            _ ->
                                pushLogStrLn
                                    loggerset
                                    (logNS utctime ERROR (show e))) >>
              executeNamed
                  conn
                  "UPDATE fbPage SET updated_time = :updated_time WHERE pid = :pid"
                  [":updated_time" := utctime, ":pid" := fbPageId fbpage])
        fbpages
    flushLogStr loggerset
