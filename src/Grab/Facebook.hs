{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Grab.Facebook
    ( grabPage
    ) where

import Data.Typeable (Typeable)
import Data.Functor ((<$>))
import Data.Aeson (toJSON, ToJSON, FromJSON)
import Data.Monoid ((<>))
import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Maybe (fromMaybe, fromJust, isJust)
import System.Log.FastLogger (LoggerSet, pushLogStrLn)
import Haxl.Core (Env, runHaxl, GenHaxl)
import Control.Monad.Extra (iterateM)
import Control.Monad.Trans.Resource (ResourceT)

import qualified Facebook as F
import qualified Facebook.Haxl as FH
import qualified Network.HTTP.Conduit as H

import NSLogger
import Grab.Facebook.FBUtils


grabPage :: LoggerSet
         -> F.AppAccessToken
         -> Env ()
         -> Text
         -> UTCTime
         -> IO ()
grabPage loggerset tok env id_ utctime = do
    page <-
        runHaxl
            env
            (FH.getPage
                 (F.Id id_)
                 [ ( "fields"
                   , "id,likes,name,talking_about_count,phone,checkins," <>
                     "category,can_post,website,is_published")]
                 tok)
    pushLogStrLn
        loggerset
        (logNS
             utctime
             INFO
             FBAction
             { _who = F.Id "someone"
             , _where = F.Id "facebook"
             , _action = "updated"
             , _what = F.pageId page
             , _entity = toJSON page
             })
    grabPagePosts loggerset tok env (F.pageId page) utctime

grabPagePosts :: LoggerSet
         -> F.AppAccessToken
         -> Env ()
         -> F.Id
         -> UTCTime
         -> IO ()
grabPagePosts loggerset tok env pageid utctime = do
    posts <-
        pagerA2listA
            env
            (FH.getPagePosts
                 pageid
                 [ ("fields", "id,shares,from,type,created_time,message")
                 , ("since", utc2bytestring (shiftTo2weeksBack utctime))]
                 tok)
    mapM
        (\p ->
              pushLogStrLn
                  loggerset
                  (logNS
                       (fromMaybe
                            utctime
                            (F.unFbUTCTime <$> F.postCreatedTime p))
                       INFO
                       FBAction
                       { _who = F.userId (fromJust (F.postFrom p))
                       , _where = pageid
                       , _action = "post"
                       , _what = F.postId p
                       , _entity = toJSON p
                       }) >>
              return (F.postId p))
        (filter
             (\p ->
                   fromMaybe utctime (F.unFbUTCTime <$> F.postCreatedTime p) >=
                   utctime)
             posts) >>=
        grabPosts loggerset tok env utctime pageid


grabPosts :: LoggerSet
          -> F.AppAccessToken
          -> Env ()
          -> UTCTime
          -> F.Id
          -> [F.Id]
          -> IO ()
grabPosts _ _ _ _ _ [] = return ()
grabPosts loggerset tok env utctime pageid postsid =
    grabPostsLikes loggerset tok env utctime pageid postsid >>
    grabPostsComments loggerset tok env utctime pageid postsid >>
    grabPostsReposts loggerset tok env utctime pageid postsid


grabPostsLikes :: LoggerSet
               -> F.AppAccessToken
               -> Env ()
               -> UTCTime
               -> F.Id
               -> [F.Id]
               -> IO ()
grabPostsLikes loggerset tok env utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA env (FH.getPostLikes postid [("fields", "id")] tok) >>=
              mapM_
                  (\liker ->
                        pushLogStrLn
                            loggerset
                            (logNS
                                 utctime
                                 INFO
                                 FBAction
                                 { _who = F.userId liker
                                 , _where = parentid
                                 , _action = "like"
                                 , _what = postid
                                 , _entity = toJSON liker
                                 })))

grabPostsComments :: LoggerSet
                  -> F.AppAccessToken
                  -> Env ()
                  -> UTCTime
                  -> F.Id
                  -> [F.Id]
                  -> IO ()
grabPostsComments loggerset tok env utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA
                  env
                  (FH.getPostComments
                       postid
                       [("fields", "id,from,created_time,like_count,message")]
                       tok) >>=
              \comments ->
                   mapM_
                       (\comment ->
                             pushLogStrLn
                                 loggerset
                                 (logNS
                                      (fromMaybe
                                           utctime
                                           (F.unFbUTCTime <$>
                                            F.commentCreatedTime comment))
                                      INFO
                                      FBAction
                                      { _who = fromMaybe
                                            (F.Id "someone")
                                            (F.userId <$> F.commentFrom comment)
                                      , _where = parentid
                                      , _action = "comment"
                                      , _what = postid
                                      , _entity = toJSON comment
                                      }))
                       (filter
                            (\comment ->
                                  fromMaybe
                                      utctime
                                      (F.unFbUTCTime <$>
                                       F.commentCreatedTime comment) >=
                                  utctime)
                            comments))


grabPostsReposts
    :: LoggerSet
    -> F.AppAccessToken
    -> Env ()
    -> UTCTime
    -> F.Id
    -> [F.Id]
    -> IO ()
grabPostsReposts loggerset tok env utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA
                  env
                  (FH.getSharedPosts
                       postid
                       [("fields", "id,from,shares,created_time,type")]
                       tok) >>=
              \reposts ->
                   mapM_
                       (\repost ->
                             pushLogStrLn
                                 loggerset
                                 (logNS
                                      utctime
                                      INFO
                                      FBAction
                                      { _who = fromMaybe
                                            (F.Id "someone")
                                            (F.userId <$> F.postFrom repost)
                                      , _where = parentid
                                      , _action = "repost"
                                      , _what = postid
                                      , _entity = toJSON repost
                                      }) >>
                             grabPosts
                                 loggerset
                                 tok
                                 env
                                 utctime
                                 (fromMaybe
                                      (F.Id "someone")
                                      (F.userId <$> F.postFrom repost))
                                 [F.postId repost])
                       (filter
                            (\repost ->
                                  fromMaybe
                                      utctime
                                      (F.unFbUTCTime <$>
                                       F.postCreatedTime repost) >=
                                  utctime)
                            reposts))


pagerA2listA
    :: (ToJSON a, FromJSON a, Show a, Typeable a)
    => Env u
    -> GenHaxl u (F.Pager a)
    -> IO [a]
pagerA2listA env getA = do
    pagerA <- runHaxl env getA
    go (Just pagerA) []
  where
    go Nothing acc = return acc
    go (Just p) acc =
        runHaxl env (FH.getNextPager p) >>=
        \ans ->
             go ans (F.pagerData p ++ acc)
