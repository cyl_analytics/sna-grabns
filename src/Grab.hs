
module Grab
    ( grabFbPage
    ) where

import Data.Text (Text)
import Data.Time (UTCTime)
import Haxl.Core (initEnv, stateSet, stateEmpty)
import System.Log.FastLogger (LoggerSet)
import Facebook (Credentials, AppAccessToken)
import Network.HTTP.Conduit (Manager)
import qualified Facebook.Haxl as F
import qualified Grab.Facebook as Fb


grabFbPage :: LoggerSet
         -> Credentials
         -> AppAccessToken
         -> Manager
         -> Text
         -> UTCTime
         -> IO ()
grabFbPage loggerset creds access_token manager pgId timestamp = do
  fbState <- F.initGlobalState 10 creds manager
  env <- initEnv (stateSet fbState stateEmpty) ()
  Fb.grabPage loggerset access_token env pgId timestamp
